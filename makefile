PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
.DEFAULT_GOAL := deptool
.PHONY: test install uninstall clean

test:
	go test ./...

deptool: deptool.go pkg/deptool/config.go pkg/deptool/recipe.go pkg/deptool/system.go pkg/deptool/web.go
	go build

install: deptool
	install -m 755 deptool "$(BINDIR)/deptool"

uninstall:
	rm -f "$(BINDIR)/deptool"
clean:
	go clean

