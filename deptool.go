/*
  MIT License
  
  Copyright (c) 2018 Jose Manuel Sanchez Madrid
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/


package main

import "os"
import "fmt"
import "flag"
import "regexp"
import "errors"
import "path/filepath"
import "deptool/pkg/deptool"

func LoadRemoteRecipe(url string, cacheDir string) (deptool.Recipe,error) {
	re := regexp.MustCompile("^(https?\\:\\/\\/)")
	destdir := filepath.Join(cacheDir,re.ReplaceAllString(url, ""))
	recipeFile := filepath.Join(destdir, "recipe.yaml")
	if _, err := os.Stat(recipeFile); err != nil {
		fmt.Fprintf(os.Stderr, "Downloading recipe '%s' into '%s'...\n",url,recipeFile)
		if destdir != "" {
			err = os.MkdirAll(destdir, os.ModePerm)
			if err != nil { return deptool.Recipe{}, err }
		}
		_, err = deptool.DownloadFile(url, recipeFile)
		if err != nil { return deptool.Recipe{}, err }
		fmt.Fprintf(os.Stderr, "Successfully downloaded '%s'.\n",recipeFile)
	} else {
		fmt.Fprintf(os.Stderr, "Recipe '%s' already found in cache '%s'. Skip downloading.\n", url, recipeFile)
	}
	return deptool.LoadFile(recipeFile)
}

func IsIn(str string, list []string) bool {
	for _,i := range list {
		if i == str { return true}
	}
	return false
}

func GenEnv(recipe deptool.Recipe,config deptool.Config) map[string]string {
	//TODO: Maybe include system environment as well
	pathvar := os.Getenv("PATH")
	if !IsIn(config.BinDir, filepath.SplitList(pathvar) ) {pathvar = fmt.Sprintf("%s:%s",pathvar,config.BinDir)}
	return map[string]string{
		"NAME":recipe.Name,
		"PKGNAME": recipe.Name,
		"VERSION": recipe.Version,
		"PREFIX":  config.Prefix,
		"SRCDIR":  config.SrcDir,
		"BINDIR":  config.BinDir,
		"LIBDIR":  config.LibDir,
		"INCDIR":  config.IncDir,
		"PKGDIR":  config.PkgDir,
		"TMPDIR":  config.TmpDir,
		"PATH":    pathvar,
	}
}

func RunAll(commands []string, env map[string]string) int {
	for _,command := range commands {
		fmt.Fprintln(os.Stderr, command)
		result := deptool.RunEnv(command, env)
		if result != 0 { return result }
	}
	return 0
}

func DownloadAll(downloads []deptool.Download, env map[string]string) error {
	mapper := func(key string)string { return env[key] }
	for _,download := range downloads {
		url := os.Expand(download.Url, mapper)
		destfile := os.Expand(download.DestFile, mapper)
		var err error
		if (destfile == "") {
			destfile, err = deptool.GetUrlFileName(url)
			if err != nil {return err}
		}

		if _, err := os.Stat(destfile); err == nil {
			fmt.Fprintf(os.Stderr,"File '%s' already downloaded into '%s'. Skipping download.\n",download.Url, destfile)
			continue
		}

		dirname, _ := filepath.Split(destfile)
		if dirname != "" {
			err = os.MkdirAll(dirname, os.ModePerm)
			if err != nil { return err }
		}

		if deptool.IsDir(destfile) {
			urlFileName, err := deptool.GetUrlFileName(url)
			if err != nil {return err}
			destfile = filepath.Join( destfile, urlFileName )
		}

		fmt.Fprintf(os.Stderr,"Downloading URL '%s' into local file '%s'.\n",download.Url, destfile)
		_,err = deptool.DownloadFile(download.Url, destfile)
		if err != nil { return err }
		fmt.Fprintf(os.Stderr,"Successfuly downloaded '%s'.\n", destfile)
	}
	return nil
}

func LoadRecipe(config deptool.Config) (deptool.Recipe,error) {
	re := regexp.MustCompile("^(https?\\:\\/\\/)")
	if re.MatchString(config.RecipeFile) {
		return LoadRemoteRecipe(config.RecipeFile,config.CacheDir)
	}
	return deptool.LoadFile(config.RecipeFile)
}

//TODO: Mybe move this to a package instead the main
func CheckAndInstall(config deptool.Config) error {
	recipe, err := LoadRecipe(config)
	if err != nil { return err }

	if config.PkgDir == "" { config.PkgDir = filepath.Join(config.SrcDir, recipe.Name, recipe.Version) }
	err = deptool.EnsureDirs([]string{config.Prefix,config.SrcDir,config.BinDir,config.LibDir,config.IncDir,config.TmpDir,config.PkgDir})
	if err != nil { return err }
	err = os.Chdir(config.PkgDir)
	if err != nil { return err }
	env := GenEnv(recipe, config)

	// Run all Checks. If all checks return 0, end execution
	result := RunAll(recipe.Check, env)
	if result == 0 {
		fmt.Fprintf(os.Stderr, "'%s' '%s' is already installed\n", recipe.Name, recipe.Version)
		return nil
	}
	fmt.Fprintf(os.Stderr,"'%s' '%s' is not installed\n",recipe.Name, recipe.Version)

	// Install Dependencies
	if len(recipe.Dependencies) > 0 { fmt.Fprintf(os.Stderr,"Installing dependencies for '%s' '%s'...\n",recipe.Name, recipe.Version) }
	for _,dependency := range recipe.Dependencies {
		fmt.Fprintf(os.Stderr, "Installing dependency '%s'...\n", dependency)
		os.Chdir(config.Cwd)
		depconfig := config
		depconfig.RecipeFile = dependency
		depconfig.PkgDir = ""
		err = CheckAndInstall(depconfig)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to install dependency '%s'.\n", dependency)
			return err
		}
		fmt.Fprintf(os.Stderr, "Successfully installed dependency '%s'.\n", dependency)
	}

	// Download
	os.Chdir(config.TmpDir)
	if len(recipe.Download) > 0 {
		fmt.Fprintf(os.Stderr, "Downloading files for '%s' '%s'...\n",recipe.Name, recipe.Version)
		err = DownloadAll(recipe.Download,env)
		if err != nil { return err }
		fmt.Fprintf(os.Stderr, "Successfully downloaded files for '%s' '%s'.\n", recipe.Name, recipe.Version)
	}

	// Run Install
	os.Chdir(config.PkgDir)
	fmt.Fprintf(os.Stderr, "Installing '%s' '%s'...\n",recipe.Name, recipe.Version)
	result = RunAll(recipe.Install, env)
	if result == 0 {
		fmt.Fprintf(os.Stderr, "'%s' '%s' installed successfully.\n",recipe.Name, recipe.Version)
		return nil
	} else {
		msg := fmt.Sprintf("'Failed to install %s' '%s'. Installation recipe returned with status code %d.\n", recipe.Name, recipe.Version, result)
		return errors.New(msg)
	}
}

func parseFlags() deptool.Config {
	config := deptool.DefaultConfig("")
	var prefix *string = flag.String("prefix", config.Prefix, "Path to install all the files. Set it to '/usr/local' if you want to make a system wide installation.")
	var validate *bool = flag.Bool("validate", config.Validate, "Validate the recipe file only, don't install it. Use this if you want to check a recipe is well formed.")
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage of %s:\n", os.Args[0])
		fmt.Fprintf(flag.CommandLine.Output(), "\n")
		fmt.Fprintf(flag.CommandLine.Output(), "  %s [options]  <recipe>\n", os.Args[0])
		fmt.Fprintf(flag.CommandLine.Output(), "\n")
		fmt.Fprintf(flag.CommandLine.Output(), "Where:\n")
		fmt.Fprintf(flag.CommandLine.Output(), "  <recipe>\n")
		fmt.Fprintf(flag.CommandLine.Output(), "        Path or URL to the recipe file.\n")
		fmt.Fprintf(flag.CommandLine.Output(), "\n")
		fmt.Fprintf(flag.CommandLine.Output(), "Options:\n")
		flag.PrintDefaults()
	}
	flag.Parse()

	if *prefix != config.Prefix { config = deptool.DefaultConfig(*prefix) }
	config.RecipeFile = flag.Arg(0)
	if config.RecipeFile == "" {
		os.Stderr.WriteString("deptool error: No recipe file given. Use '-h' to see comand usage.\n")
		os.Exit(2)
	}
	config.Validate = *validate
	return config
}

func main() {
	config := parseFlags()

	if config.Validate {
		_, err := LoadRecipe(config)
		if err != nil {
			fmt.Fprintf(os.Stdout, "ERROR %s\n", config.RecipeFile)
			fmt.Fprintf(os.Stderr,"%v\n",err)
			os.Exit(1)
		}
		fmt.Fprintf(os.Stdout, "VALID %s\n", config.RecipeFile)
		os.Exit(0)
	}

	err := CheckAndInstall(config)
	if err != nil {
		fmt.Fprintf(os.Stderr,"%v\n",err)
		fmt.Fprintf(os.Stderr,"Aborted installation of recipe '%s'.\n", config.RecipeFile)
		os.Exit(1)
	}
	fmt.Fprintf(os.Stderr,"Successfully installed recipe '%s'.\n", config.RecipeFile) //TODO:  Maybe use log instead prints
	os.Exit(0)
}
