/*
  MIT License
  
  Copyright (c) 2018 Jose Manuel Sanchez Madrid
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/


package deptool

import "fmt"
import "os"
import "os/exec"

func RunEnv(command string, env map[string]string) int {
	cmd := exec.Command("/bin/sh","-c", command)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if (env != nil) {
		vars := os.Environ()
		for key,value := range env {
			vars = append(vars, fmt.Sprintf("%s=%s",key,value))
		}
		cmd.Env = vars
	}
	cmd.Run()
	return cmd.ProcessState.ExitCode()
}

func EnsureDirs(directories []string) error {
	for _, directory := range directories {
		err := os.MkdirAll(directory, os.ModePerm) //TODO: check perms
		if  err != nil { return err }
	}
	return nil
}

func IsDir(path string) bool {
	info, err := os.Stat(path)
	if os.IsNotExist(err) { return false }
	return info.IsDir()
}

