/*
  MIT License
  
  Copyright (c) 2018 Jose Manuel Sanchez Madrid
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/


package deptool

import "testing"
import "github.com/stretchr/testify/assert"
import "github.com/stretchr/testify/require"

/*
func TestLoadFile(t *testing.T) {
	expected :=  Recipe{
		Name: "zlib",
		Version: "1.2.11",
		Dependencies: nil,
		Check: []string{
				"test -r ${LIBDIR}/libz.so",
				"test -r ${INCDIR}/zlib.h",
		},
		Download: []Download{
				{"https://github.com/madler/zlib/archive/v1.2.11.tar.gz", "${TMPDIR}/libz.tgz"},
		},
		Install: []string{
				"rm -rf ./*",
				"tar xzf \"${TMPDIR}/libz.tgz\" && mv ${PKGDIR}/zlib-${VERSION}/* ${PKGDIR}/",
				"./configure --prefix=${PREFIX}",
				"make",
				"make install",
		},
	}
	result, err := LoadFile("testing.yaml")
	require.NoError(t, err)
	assert.Equal(t, expected, result)
}
*/

func TestParseRecipe(t *testing.T) {
	given := []byte(`name: zlib
version: 1.2.11
check:
  - test -r ${LIBDIR}/libz.so
  - test -r ${INCDIR}/zlib.h
download: https://github.com/madler/zlib/archive/v1.2.11.tar.gz ${TMPDIR}/libz.tgz
install: 
  - rm -rf ./*
  - tar xzf "${TMPDIR}/libz.tgz" && mv ${PKGDIR}/zlib-${VERSION}/* ${PKGDIR}/
  - ./configure --prefix=${PREFIX}
  - make
  - make install
`)
	expected :=  Recipe{
		Name: "zlib",
		Version: "1.2.11",
		Dependencies: nil,
		Check: []string{
				"test -r ${LIBDIR}/libz.so",
				"test -r ${INCDIR}/zlib.h",
		},
		Download: []Download{
				{"https://github.com/madler/zlib/archive/v1.2.11.tar.gz", "${TMPDIR}/libz.tgz"},
		},
		Install: []string{
				"rm -rf ./*",
				"tar xzf \"${TMPDIR}/libz.tgz\" && mv ${PKGDIR}/zlib-${VERSION}/* ${PKGDIR}/",
				"./configure --prefix=${PREFIX}",
				"make",
				"make install",
		},
	}

	result, err := ParseRecipe(given)
	require.NoError(t, err)
	assert.Equal(t, expected, result)
}

func TestParseRecipeMinimumNameAndVersion(t *testing.T) {
	given := []byte(`name: zlib
version: 1.2.11`)

	expected :=  Recipe{
		Name: "zlib",
		Version: "1.2.11",
	}

	result, err := ParseRecipe(given)
	require.NoError(t, err)
	assert.Equal(t, expected, result)
}

func TestParseRecipeFails(t *testing.T) {
	given := []byte("This is not a valid yaml")

	_, err := ParseRecipe(given)
	assert.Error(t,err)
}

func TestParseRecipeRequiresName(t *testing.T) {
	given := []byte(`version: 1.2.11`)

	_, err := ParseRecipe(given)
	assert.Error(t, err)
}

func TestParseRecieRequiresVersion(t *testing.T) {
	given := []byte(`name: zlib`)

	_, err := ParseRecipe(given)
	assert.Error(t,err)
}

func TestParseRecipeSingleStringInsteadList(t *testing.T) {
	given := []byte(`name: zlib
version: 1.2.11
check: test -r ${LIBDIR}/libz.so
download: https://github.com/madler/zlib/archive/v1.2.11.tar.gz ${TMPDIR}/libz.tgz
install: tar xzf ${TMPDIR}/libz.tgz && ./configure && make install
dependencies: https://example.tld/example.yaml
`)
	expected :=  Recipe{
		Name: "zlib",
		Version: "1.2.11",
		Dependencies: []string{
			"https://example.tld/example.yaml",
		},
		Check: []string{
			"test -r ${LIBDIR}/libz.so",
		},
		Download: []Download{
			{"https://github.com/madler/zlib/archive/v1.2.11.tar.gz", "${TMPDIR}/libz.tgz"},
		},
		Install: []string{
			"tar xzf ${TMPDIR}/libz.tgz && ./configure && make install",
		},
	}

	result, err := ParseRecipe(given)
	require.NoError(t, err)
	assert.Equal(t, expected, result)
}

func TestParseRecipeLists(t *testing.T) {
	given := []byte(`name: zlib
version: 1.2.11
check:
  - test -r ${LIBDIR}/libz.so
  - test -r ${INCDIR}/zlib.h
download:
  - https://github.com/madler/zlib/archive/v1.2.11.tar.gz ${TMPDIR}/libz.tgz
  - https://example.tld/something.txt ${TMPDIR}/something.txt
install: 
  - rm -rf ./*
  - tar xzf "${TMPDIR}/libz.tgz" && mv ${PKGDIR}/zlib-${VERSION}/* ${PKGDIR}/
  - ./configure --prefix=${PREFIX}
  - make
  - make install
dependencies:
  - https://example.tld/example1.yaml
  - https://example.tld/example2.yaml
`)
	expected :=  Recipe{
		Name: "zlib",
		Version: "1.2.11",
		Dependencies: []string{
			"https://example.tld/example1.yaml",
			"https://example.tld/example2.yaml",
		},
		Check: []string{
			"test -r ${LIBDIR}/libz.so",
			"test -r ${INCDIR}/zlib.h",
		},
		Download: []Download{
			{"https://github.com/madler/zlib/archive/v1.2.11.tar.gz", "${TMPDIR}/libz.tgz"},
			{"https://example.tld/something.txt", "${TMPDIR}/something.txt"},
		},
		Install: []string{
			"rm -rf ./*",
			"tar xzf \"${TMPDIR}/libz.tgz\" && mv ${PKGDIR}/zlib-${VERSION}/* ${PKGDIR}/",
			"./configure --prefix=${PREFIX}",
			"make",
			"make install",
		},
	}

	result, err := ParseRecipe(given)
	require.NoError(t, err)
	assert.Equal(t, expected, result)
}
