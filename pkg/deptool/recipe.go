/*
  MIT License
  
  Copyright (c) 2018 Jose Manuel Sanchez Madrid
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/


package deptool

import "gopkg.in/yaml.v2"
import "errors"
import "strings"
import "io/ioutil"


type Download struct {
	Url string
	DestFile string
}

type Recipe struct {
	Name string
	Version string
	Check []string
	Install []string
	Download []Download
	Dependencies []string
}


func ParseRecipe(bytes []byte) (Recipe, error) {
	result := Recipe{}
	var parsed map[string]interface{}
	err := yaml.Unmarshal(bytes, &parsed)
	if err != nil {
		return result, err
	}
	var ok bool
	result.Name, ok = parsed["name"].(string)
	if !ok {
		return result, errors.New("name must be a string")
	}
	if result.Name == "" {
		return result, errors.New("name must be not empty")
	}
	result.Version, ok = parsed["version"].(string)
	if !ok {
		return result, errors.New("version must be a string")
	}
	if result.Version == "" {
		return result, errors.New("name must be not empty")
	}
	if _, ok = parsed["check"]; ok {
		if value, ok := parsed["check"].(string); ok {
			result.Check = []string{value}
		} else if value, ok := parsed["check"].([]interface{}); ok {
			result.Check = []string{}
			for _, line := range value {
				strline, ok := line.(string)
				if !ok {
					return result, errors.New("Check must be a string or a list of strings")
				}
				result.Check = append(result.Check, strline)
			}
		} else {
			return result, errors.New("check must be an string or a list of strings")
		}
	}
	if _, ok = parsed["install"]; ok {
		if value, ok := parsed["install"].(string); ok {
			result.Install = []string{value}
		} else if value, ok := parsed["install"].([]interface{}); ok {
			result.Install = []string{}
			for _, line := range value {
				strline, ok := line.(string)
				if !ok {
					return result, errors.New("install must be a string or a list of strings")
				}
				result.Install = append(result.Install, strline)
			}
		} else {
			return result, errors.New("install must be an string or a list of strings")
		}
	}
	if _, ok = parsed["dependencies"]; ok {
		if value, ok := parsed["dependencies"].(string); ok {
			result.Dependencies = []string{value}
		} else if value, ok := parsed["dependencies"].([]interface{}); ok {
			result.Dependencies = []string{}
			for _, line := range value {
				strline, ok := line.(string)
				if !ok {
					return result, errors.New("dependencies must be a string or a list of strings")
				}
				result.Dependencies = append(result.Dependencies, strline)
			}
		} else {
			return result, errors.New("dependencies must be an string or a list of strings")
		}
	}
	if _, ok = parsed["download"]; ok {
		if value, ok := parsed["download"].(string); ok {
			tokens := strings.SplitN(value, " ", 2)
			download := Download{}
			download.Url = tokens[0]
			if len(tokens) > 1 {
				download.DestFile = strings.TrimSpace(tokens[1])
			}
			result.Download = []Download{download}
		} else if value, ok := parsed["download"].([]interface{}); ok {
			result.Download = []Download{}
			for _, line := range value {
				strline, ok := line.(string)
				if !ok {
					return result, errors.New("download must be a string or a list of strings")
				}
				tokens := strings.SplitN(strline, " ", 2)
				download := Download{}
				download.Url = tokens[0]
				if len(tokens) > 1 {
					download.DestFile = strings.TrimSpace(tokens[1])
				}
				result.Download = append(result.Download, download)
			}
		} else {
			return result, errors.New("download must be an string or a list of strings")
		}
	}
	return result, nil
}

func LoadFile(filePath string) (Recipe, error) {
	bytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		return Recipe{}, err
	}
	return ParseRecipe(bytes)
}
